(function () {
    'use strict';

    angular
        .module('capvalue.job', [
            'capvalue.job.controllers',
            'capvalue.job.services'
        ]);

    angular
        .module('capvalue.job.controllers', []);

    angular
        .module('capvalue.job.services', []);
})();